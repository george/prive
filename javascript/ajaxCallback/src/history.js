import { spip } from "config.js";
import { ajaxClick } from "./ajaxbloc.js";
import { log } from "./log.js";

export function setHistoryState(blocfrag) {
	if (!window.history.replaceState) return;
	// attribuer un id au bloc si il n'en a pas
	if (!blocfrag.getAttribute("id")) {
		spip.stateId = [...document.querySelectorAll('[id^="ghsid"]')].length;
		blocfrag.setAttribute("id", `ghsid${++spip.stateId}`);
	}
	const href = blocfrag.dataset.url || blocfrag.dataset.origin;
	//href = document.querySelector('a[href="'+href+'"]');
	//log(href);
	const state = {
		id: blocfrag.getAttribute("id"),
		href: href,
	};
	const ajaxid = blocfrag.getAttribute("class").match(/\bajax-id-[\w-]+\b/);
	if (ajaxid?.length) {
		state.ajaxid = ajaxid[0];
	}
	// on remplace la variable qui decrit l'etat courant
	// initialement vide
	// -> elle servira a revenir dans l'etat courant
	window.history.replaceState(
		state,
		window.document.title,
		window.document.location,
	);

}

export function pushHistoryState(href, title) {
	if (!window.history.pushState) {
		return false;
	}
	window.history.pushState({}, title, href);
}

window.onpopstate = (popState) => {
  if (popState.state?.href) {
    log(["popState", popState]);
    let blocfrag = false;
    for (const bloc_id of popState.state.id.split(" ")) {
      blocfrag = document.getElementById(bloc_id);
      ajaxClick(blocfrag, popState.state.href, { history: false });
		// si on revient apres avoir rompu la chaine ajax, on a pu perdre l'id #ghsidxx ajoute en JS
		// dans ce cas on redirige hors ajax
		if (!blocfrag) {
			window.location.href = popState.state.href;
			break;
		}
    }
  }
};

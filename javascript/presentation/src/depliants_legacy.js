/**
 * Ce module reprend quasiment à l'identique, le code historique
 * pour la gestion des icones dépliables (click et survol) des rubriques
 *
 * Il est probable que le code soit remanié (ou supprimé) prochainement.
 * Aussi il est déconseillé de le réutiliser en l'état.
 */
import { slideDown, slideUp, throttle } from "ajaxCallback.js";

function depliant(origine, _cible) {
	if (!origine.classList.contains("depliant")) {
		const time = 400;
		let t = null;
		const cible = _cible || document.querySelector(origine.dataset.depliant);
		origine.classList.add("depliant");
		origine.addEventListener(
			"mouseenter",
			throttle(() => {
				if (origine.classList.contains("replie")) {
					t = setTimeout(() => {
						t = null;
						showother(origine, cible);
					}, time);
				}
			}, 200),
		);
		origine.addEventListener(
			"mouseleave",
			throttle(() => {
				if (t) {
					clearTimeout(t);
					t = null;
				}
			}, 200),
		);
	}
}
async function showother(origine, cible) {
	if (origine.classList.contains("togglewait")) {
		return false;
	}
	if (origine.classList.contains("replie")) {
		origine.classList.remove("replie");
		origine.classList.add(...["deplie", "togglewait"]);
		await slideDown(cible);
		cible.classList.add("blocdeplie");
		cible.classList.remove("blocreplie");
		origine.classList.remove("togglewait");
	}
}
async function hideother(origine, cible) {
	if (origine.classList.contains("togglewait")) {
		return false;
	}
	if (!origine.classList.contains("replie")) {
		origine.classList.remove("deplie");
		origine.classList.add(...["replie", "togglewait"]);
		await slideUp(cible);
		cible.classList.add("blocreplie");
		cible.classList.remove("blocdeplie");
		origine.classList.remove("togglewait");
	}
}

function toggleother(origine, cible) {
	return origine.classList.contains("replie")
		? showother(origine, cible)
		: hideother(origine, cible);
}

export function activer_depliants(_root) {
	const root = _root || document;
	for (const depliant_survol of [
		...root.querySelectorAll("[data-depliant]"),
	].filter((e) => !e.classList.contains("depliant"))) {
		depliant(depliant_survol);
		if (depliant_survol.dataset.fixDepliIncertain !== "undefined") {
			const cible = document.querySelector(depliant_survol.dataset.depliant);
			if (cible?.offsetParent) {
				depliant_survol.classList.add("deplie");
				depliant_survol.classList.remove("replie");
			}
		}
	}
	for (const btn_clicancre of [
		...root.querySelectorAll("[data-depliant-clicancre]"),
	].filter((e) => !e.classList.contains("depliant"))) {
		["click", "keydown"].forEach((evt) => {
			btn_clicancre.addEventListener(
				evt,
				throttle((e) => {
					e.preventDefault();
					btn_clicancre.classList.add("depliant");
					const origine =
						e.target.parentNode.dataset.depliant !== "undefined"
							? e.target.parentNode
							: btn_clicancre;
					const cible =
						document.querySelector(btn_clicancre.dataset.depliantClicancre) ||
						document.querySelector(origine.dataset.depliant);
					toggleother(origine, cible);
				}, 250),
			);
		});
	}
}

import { spip } from "config.js";
import { parametre_url } from "./url.js";

spip.debug =
	spip.debug ||
	!!(parametre_url(window.location.href, "var_mode") === "debug_js");

export function log(...args) {
	if (spip.debug && window?.console?.log) {
		window.console.log.apply(null, args);
	}
}

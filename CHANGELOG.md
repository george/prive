# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Security

- spip-security/securite#4860 bien tester les autorisations d'afficher le contenu des articles/rubriques dans les fragments chargés en ajax

### Added

 - #56: (style) définir une hauteur minimale pour certains blocs qui reçoivent du contenu de manière asynchrone

### Changed

 - #66: `login.js` n'a pas besoin de jQuery
 - #56: `admin_vider_images` n'a pas besoin de jQuery

### Fixed

- #63: harmoniser l'affichage de la barre d'onglets et de la boite d'infos sur les pages préférences auteur
- #56: appliquer `endLoading()` après les requêtes asynchrones de la page `admin_vider`
- #54: l'apparence du sélecteur (picker) est plus cohérente avec le thème du privé
- Suite à spip/spip/6094 & spip/images!4735 : ne plus lister en dur les process d’image dans le formulaire de configuration
- JS: Correction sur le retour des formulaires ajaxés
- JS: améliorer les styles embarqués par défaut pour éviter les sautillements visuels durant le `.loading`
- JS: améliorer le comportement de `positionner()`
- #18 Transmettre au pipeline `compter_contributions_auteur` le nombre de contribution sur les articles
- #18 Utiliser le pipeline `compter_contributions_auteur` dans la boite des infos d’auteurs

### Removed

- spip/porte_plume#4835 suppression du fichier migré vers porte-plume `spip_barre.js`
- !62: suppression du fichier `jquery.form.js`
- #55: retrait de propriétés CSS obsolètes / inutiles
- #50: Le flux RSS des brèves est déplacé dans le plugin Brèves

## 2.0.0-beta - 2024-12-03

### Added

- spip/spip#6043:
  ├── Nouvelle méthode de chargement automatique et ordonné pour l'initialisation JS (ex.: `javascript/_initjs/10_player.js`)
  ├── un paramètre d'url`?var_mode=debug_js` active le mode verbeux dans la console JS
  ├── ajout de `javascript/config.js.html`, point de départ pour générer les options de configuration côté navigateur
  └── ajout de `javascript/retrocompat.js` pour tenter d'assurer une certaine retro-compatibilité
- !20: les valeurs d'environnement explicitement vidées en ajax passent par `var_nullify` pour en être totalement expurgées
- !16: Suppression de la constante _DIR_RESTREINT_ABS
- !10: Permettre de trier les visiteurs 'nouveau' par date d'inscription-relance
- #3: Ajouter l'heure de publication à côté de la date
- !1: Utiliser des variables CSS dans l’espace privé pour éviter la compilation des fichiers CSS
- Composerisation

### Changed

- #spip/ecrire#20 `formulaire_bouton_action_post` prend en compte le markup `data-callback` en remplacement de `onclick`
- spip/spip#6043
  ├── `ajaxCallback.js` réécrit en javascript natif (ESM), sa modification nécessite une compilation (ex.: `bun run build`)
  ├── on accède désormais à l'objet (ex `spipConfig`) via la syntaxe par module `import { default as spip } from "config.js";`
  ├── `animateLoading()`, `endLoading()`, `animateRemove()`, `animateAppend()` utilisent des animations CSS (fichier `ajax.css`) et s'émancipent de jQuery
  ├── `positionner()` implémente la fonction native `scrollIntoView`
  ├── la variable CSS `--scroll-margin` définit la marge de dégagement lors du repositionnement
  ├── `formulaire_configurer_preferences`, `formulaire_editer-liens`,`admin_vider_cache` n'ont pas besoin de jQuery
  └── `presentation.js` devient un module ESM pour certaines fonctionnalités du privé (logo_survol,puce_survol,blocs_depliants,reloadExecPage)
- spip/medias#4958 Utilisation de `image_extensions_logos()` à la place de `$GLOBALS['formats_logos']`

### Fixed

- #46 Traiter sans erreur `ajaxReload('mon_bloc')`lorsque `mon_bloc` n'est pas `.ajaxbloc`
- !36 Passer l'url à l'item de langue `pass_reset_url`
- #1 Pouvoir retirer des liens sur un objet lorsqu'on peut le modifier
- !24 pour le formulaire générique d'institution d'objet, tester l'autorisation `publierdans` en utilisant le parent déclaré par l'API de parenté
- spip/spip#3408 Dans le formulaire générique d'institution d'un objet, utiliser le même jeu de test pour l'affichage du statut `publie` dans `charger` et pour la validation dans `verifier`
- #14 Pouvoir supprimer l'image de l'écran de connexion
- !13 Pouvoir modifier logo principal quand il y a un logo de survol
- spip/spip#3928 Les emails des auteurs sont masqués par défaut


### Removed

- !45 retrait du flux RSS des brèves `#URL_PAGE{backend-breves}`
- spip/spip#5975 retrait de `prefixfree.js`
- spip/spip#6043
  ├──`jQuery.spip.intercepted.xxx`
  └──`jQuery.spip.positionner_marge` : l'ajustement de la marge verticale est à personnaliser manuellement via la règle CSS : `scroll-margin`

### Deprecated

- spip/spip#6043 `jQuery.uaMatch`,`jQuery.browser`

## Description du bug / Comment le reproduire

1. ...
2. ...
3. ...

## Comportement attendu / Comportement obtenu

_Qu'est ce que vous vous attendez à obtenir ?_\
_Quel est le résultat réellement obtenu ?_

## Piste de résolution

_Si vous avez une piste pour résoudre, sinon effacez cette section_

## Autres informations

_Si nécessaire, vous pouvez ajouter_ :
- _url dans l'espace privé /ecrire/?exec=..._
- _url du site public si le bug y est visible / reproductible,_
- _captures écrans,_
- _extraits de logs,_

_sinon effacez cette section_

## Informations techniques

* Version de SPIP :
* Version de PHP :
* Navigateur utilisé : Nom et version
* Serveur web (si pertinent) : Apache, Nginx, ...
* Système d'exploitation (si pertinent) :
* Base de données (si pertinent) : MySQL/MariaDb/Sqlite + numéro de version

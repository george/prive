import { test, expect } from "bun:test";
import { onAjaxLoad, triggerAjaxLoad } from "../src/ajaxbloc.js";

document.body.innerHTML = `
<div class="item"></div>
<div class="item"></div>
<div class="item"></div>
<div class="dummy_ajaxbloc">
	<div class="item"></div>
	<div class="item"></div>
	<div class="item"></div>
</div>
`;

let compteur = 0;

const compter_items = (_bloc) => {
	const bloc = _bloc || document;
	compteur = [...bloc.querySelectorAll('.item')].length;
}

test("onAjaxLoad(func) : func reçoit le bloc de référence issu de triggerAjaxload() comme premier argument", () => {
	expect(compteur).toBe(0);
	compter_items();
	expect(compteur).toBe(6);
	onAjaxLoad(compter_items);
	triggerAjaxLoad(document.querySelector('.dummy_ajaxbloc'));
	expect(compteur).toBe(3);
});

